#!/usr/bin/perl
###################
### Read website utrecht.nl en try to scrape info
###
### 2020-10-29 - Sjoerd de Boer - Initial Version
##
##
use LWP::UserAgent;
use DBI;
use Data::Dumper;

require HTTP::Cookies;

my $ua = new LWP::UserAgent;
#$ua->agent('PirateBot/1.0');
$ua->agent('Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0');
$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME}=0;
$ua->ssl_opts( verify_hostname => 0 ,SSL_verify_mode => 0x00);


open(OVERZICHT,">", "Overzicht_2018.txt") or die $!;

sub WOB 
{
   $WOB = $_[0];
   $WOB = "https://archief12.archiefweb.eu/archives/archiefweb/20190101151443/".$WOB;
   my $req = new HTTP::Request('GET',$WOB);
   my $res = $ua->request($req);
   my $content=$res->content;
   my $H1_pos1=index($content,"<h1>")+4;
   my $H1_pos2=index($content,"</h1>");
   my $H1 = substr($content,$H1_pos1,($H1_pos2-$H1_pos1));
   my $WOB_NR = substr($H1,0,9);
   my $WOB_DESCR = substr($H1,9);
   print OVERZICHT "Wob = $WOB_NR - $WOB_DESCR\n";
   if (-d $WOB_NR) { } else {  mkdir $WOB_NR; }
   my $pre = "/fileadmin/uploads/";
   my $pos = index($content,$pre);
   while ( $pos > 0 )
   {
      $content = substr($content,$pos);
      my $pos2 = index($content,'"');
      print "POS : $pos = POS2 : $pos2\n";
      $PDF_File = substr($content,0,$pos2);
      print "PDF: $PDF_File\n"; 
      $PDF_File = "https://archief12.archiefweb.eu/archives/archiefweb/20190101151443/https://www.utrecht.nl".$PDF_File;
      my $pos3 = rindex($PDF_File,'/');
      my $file_name =  substr($PDF_File,$pos3+1);
      my $download = $ua->get($PDF_File);
      print "Downloading $file_name for WOB $WOB_NR\n";
      open SAVE, ">", $WOB_NR."/".$file_name;
      print SAVE $download->content;
      close SAVE;
      $content = substr($content,5);
      $pos = index($content,$pre);
   }
   ##print Dumper($content);
   ##exit 0;
}

my $Main_URL = "https://archief12.archiefweb.eu/archives/archiefweb/20190101151443/https://www.utrecht.nl/bestuur-en-organisatie/publicaties/openbaar-gemaakte-informatie-na-wob-verzoeken/page/";

my $page = 1;

while ( $page < 90 )
{
  my $uri = $Main_URL.$page;
  my $req = new HTTP::Request('GET',$uri);
  my $res = $ua->request($req);
  my $content=$res->content;
  my $positie = index($content,'href');
  my $prefix = "://www.utrecht.nl/bestuur-en-organisatie/publicaties/openbaar-gemaakte-informatie-na-wob-verzoeken/wob-verzoek/2018"; 

  while ( $positie > 0 ) 
    {
       $content = substr($content,$positie+6);
       if (substr($content,0,121) =~ m/$prefix/ )
       {
       my $tag = index($content,'">2018');
       my $found = substr($content,0,$tag);
       print "F: $found\n";
       WOB ($found);
       }
    $positie = index($content,'href');
    }
    $page++;
}




